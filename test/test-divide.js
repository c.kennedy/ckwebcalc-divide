var expect  = require('chai').expect;
var div = require('../divide');

it('Divide Test', function(done) {
        var x = 20;
        var y = 5;
        var a = x / y;
        expect(div.divide(x,y)).to.equal(a);
        done();
});
